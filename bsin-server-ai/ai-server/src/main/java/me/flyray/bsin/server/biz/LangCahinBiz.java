package me.flyray.bsin.server.biz;

import dev.langchain4j.memory.chat.TokenWindowChatMemory;
import dev.langchain4j.model.openai.OpenAiChatModel;
import dev.langchain4j.model.openai.OpenAiTokenizer;
import dev.langchain4j.service.AiServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * @author ：leonard
 * @date ：Created in 2023/10/20 16:25
 * @description：
 * @modified By：
 */

@Slf4j
@Component
public class LangCahinBiz {
    //    @Value("${langchain4j.chat-model.openai.api-key}")
    private String OPENAI_API_KEY = "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";

    private Assistant assistant;

    interface Assistant {
        String chat(String message);
    }

    LangCahinBiz() {
        log.info("LangCahin4JBiz Init................................................................................\n\n\n\n\n\n\n");
        this.init();
    }

    public void init() {
        var memory = TokenWindowChatMemory.withMaxTokens(1000, new OpenAiTokenizer("gpt-3.5-turbo"));
        assistant =
                AiServices.builder(Assistant.class)
                        .chatLanguageModel(OpenAiChatModel.withApiKey(OPENAI_API_KEY))
                        .chatMemory(memory)
                        .build();
    }

    public String chat(String message) {
        return assistant.chat(message);
    }
}
