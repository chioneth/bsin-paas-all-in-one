package me.flyray.bsin.domain;

import java.io.Serializable;

import lombok.Data;

/**
 * @author HLW
 **/
@Data
public class LoginUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客户NO
     */
    private String customerNo;


    /**
     * 租户ID
     */
    private String tenantId;


    /**
     * 管理平台用户ID
     */
    private String userId;


    /**
     * 管理平台用户名
     */
    private String userName;

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 商户ID
     */
    private String merchantNo;


    /**
     * 用户类型
     */
    private String customerType;


    /**
     * 租户类型
     */
    private String tenantAppType;

    /**
     * 店铺ID
     */
    private String storeNo;


    /**
     * 创建者
     */
    private String createBy;


    /**
     * 更新者
     */
    private String updateBy;


    /**
     * token
     */
    private String token;
}
