package me.flyray.bsin.server.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "me.flyray.bsin.tenant")
public class TenantConfig {

    private String password;

    private String postName;

    private String appId;

    private String daobookAppId;

    private String jiujiuAppId;

    private String workflowAppId;

    private String workflowRoleId;

    private String roleName;

    private String bizRoleName;

    private String roleId;

    private Integer roleType;

}
