package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("processInstance")
public interface BsinAdminProcessInstanceService {

    /**
     * 流程实例挂起
     * @param requestMap
     * @return
     */
    @POST
    @Path("suspendProcessInstance")
    @Produces("application/json")
    Map<String, Object> suspendProcessInstance(Map<String, Object> requestMap);

    /**
     * 激活流程实例
     * @param requestMap
     * @return
     */
    @POST
    @Path("activateProcessInstance")
    @Produces("application/json")
    Map<String, Object> activateProcessInstance(Map<String, Object> requestMap);

    /**
     * 分页查询流程实例
     * @param requestMap
     * @return
     */
    @POST
    @Path("getPageListProcessInstance")
    @Produces("application/json")
    Map<String, Object> getPageListProcessInstance(Map<String, Object> requestMap);

    /**
     * 分页查询流程实例
     * @param requestMap
     * @return
     */
    @POST
    @Path("getMyProcessInstancePageList")
    @Produces("application/json")
    Map<String, Object> getMyProcessInstancePageList(Map<String, Object> requestMap);

    /**
     * 流程实例预览
     * @param requestMap
     * @return
     */
    @POST
    @Path("processInstancePreview")
    @Produces("application/json")
    Map<String, Object> processInstancePreview(Map<String, Object> requestMap);

    /**
     * 流程实例预览
     * @param requestMap
     * @return
     */
    @POST
    @Path("getActInstList")
    @Produces("application/json")
    Map<String, Object> getActInstList(Map<String, Object> requestMap);
}
