import { createContainer } from 'unstated-next';
import { useState, useEffect } from 'react';
import { onChangeTheme } from '@/utils/changeTheme';
import {
  setLocalStorageInfo,
  getLocalStorageInfo,
} from '@/utils/localStorageInfo';

export type AppMenu = {
  title: string;
  menuName: string;
  icon: string;
  to: string;
  children?: AppMenu[];
}[];


type InitialState = {
  userInfo: any;
  appMenu?: AppMenu;
};

/**
 * 管理 layouts 中的数据状态
 * @returns
 */
const BsinLayoutContext = (initialState: InitialState | undefined) => {
  // 用户信息
  const [userInfo, setUserInfo] = useState(
    initialState ? initialState.userInfo : 'admin',
  );

  // 子应用数据
  const [appMenu, setAppMenu] = useState(
    initialState ? initialState.appMenu : undefined,
  );

  /**
   * 监听页面路径设置布局
   */
  useEffect(() => {
    // 工作台layout不变
    
  }, [location.hash]);


  /**
   * 更新用户信息
   */
  const changeUserInfo = () => {
    let userInfo = getLocalStorageInfo('userInformation');
    setLocalStorageInfo('userInformation', userInfo);
  };

  // 改变子应用菜单
  useEffect(() => {
    setAppMenu(initialState?.appMenu);
  }, [initialState?.appMenu]);

  return {
    appMenu,
    userInfo,
  };
};

const BsinLayoutCounter = createContainer(BsinLayoutContext);

export default BsinLayoutCounter;
