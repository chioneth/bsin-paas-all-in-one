package me.flyray.bsin.gateway.service;

import com.alipay.sofa.common.utils.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.log4j.Log4j2;
import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.enums.CustomerType;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.gateway.common.ApiResult;
import me.flyray.bsin.gateway.context.BaseContextHandler;
import me.flyray.bsin.gateway.context.BsinContextBuilder;
import me.flyray.bsin.oss.ipfs.BsinIpfsService;
import me.flyray.bsin.utils.BsinServiceInvokeUtil;

@Log4j2
@Service
public class BsinIpfsRequestHandlerService {

    @Value("${bsin.s11edao.ipfs.api}")
    private String ipfsApi;
    @Value("${bsin.s11edao.ipfs.gateway}")
    private String ipfsGateway;
    @Autowired
    private BsinServiceInvokeUtil bsinServiceInvokeUtil;
    @Autowired
    private BsinIpfsService bsinIpfsService;

    public ApiResult ipfsRequestHandler(String methodName, Map<String, Object> bizParams) throws IOException {
        String currentPath = (String) bizParams.get("currentPath");
        if (StringUtil.isBlank(currentPath)) {
            currentPath = "";
        }
        if ("makeDirectory".equals(methodName)) {
            String fileName = (String) bizParams.get("fileName");
            String fileCode = (String) bizParams.get("fileCode");
            String fileDescription = (String) bizParams.get("fileDescription");
            String tenantId = (String) BaseContextHandler.get("tenantId");
            String customerNo = (String) BaseContextHandler.get("customerNo");
            String customerType = (String) BaseContextHandler.get("customerType");
            // 根据用户类型判断用户属于哪个平台
            String appFlag = "bigan";
            if (CustomerType.TENANT.getCode().equals(customerType)) {
                appFlag = "daobook";
            }
            String dirPath = "/" + appFlag + "/" + tenantId + currentPath + "/" + fileName;
            try {
                // TODO 判断文件夹是否存在
                bsinIpfsService.mkDir("/" + appFlag + "/" + tenantId);
            } catch (Exception e) {
                log.error(e.toString());
            }
            String fileAddress;
            try {
                fileAddress = bsinIpfsService.mkDir(dirPath);
            } catch (Exception e) {
                log.error(e.toString());
                // 文件夹名称重复
                throw new BusinessException(ResponseCode.IPFS_DIR_IS_EXISTS);
            }
            // 创建数据库数据
            Map<String, Object> requestMap = new HashMap<>();

            Map<String, Object> loginMap = BsinContextBuilder.buildLoginContextMap();
            if (ObjectUtil.isNotEmpty(loginMap)) {
                requestMap.put("headers", loginMap);
            }

            requestMap.put("tenantId", tenantId);
            requestMap.put("fileName", fileName);
            requestMap.put("customerNo", customerNo);
            requestMap.put("fileUrl", fileAddress);
            requestMap.put("fileCode", fileCode);
            requestMap.put("fileDescription", fileDescription);

            Map map = bsinServiceInvokeUtil.genericInvoke("MetadataFileService", "makeDirectory", "1.0", requestMap);

            return ApiResult.ok();
        }

        /*if ("ipfsLs".equals(methodName)) {
            String tenantId = (String) BaseContextHandler.get("tenantId");
            String customerType = (String) BaseContextHandler.get("customerType");
            // 根据用户类型判断用户属于哪个平台
            String dev = "bigan";
            if (CustomerType.TENANT_DAO.getCode().equals(customerType)) {
                dev = "daobook";
            }

            if (currentPath == null || currentPath.isEmpty()) {
                currentPath = "/" + dev + "/" + tenantId;
            } else {
                currentPath = "/" + dev + "/" + tenantId + "/" + currentPath;
            }

            String hashDir = this.fileStat(currentPath).get("Hash").toString();
            JSONObject result = this.fileLS(hashDir);
            return ApiResult.ok(result);
        }*/
        return null;
    }
}
