package me.flyray.bsin.gateway;

import io.ipfs.api.IPFS;

import java.io.*;


import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class IpfsTest {

    private static IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
    private static String ipfHttpUrl = "http://127.0.0.1:5001/api/v0/";
//    private static String ipfHttpUrl = "http://114.116.93.253:5002/api/v0/";


    @Test
    public void add() throws IOException {
        NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(new File("/home/leonard/code/metadata/test.txt"));
        MerkleNode addResult = ipfs.add(file).get(0);
        System.out.println(String.format("%s文件上传成功,Hash值为: %s", (Object) file.getName(), addResult.toString()));
    }

    @Test
    public void cat() throws IOException {
        String hash = "QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH";
        String filename = "./download.txt";
        Multihash filePointer = Multihash.fromBase58(hash);
        byte[] data = ipfs.cat(filePointer);
        if (data != null) {
            File file = new File(filename);
            if (file.exists()) {
                System.out.println("file exist");
                file.delete();
            }
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data, 0, data.length);
            fos.flush();
            fos.close();
        }
    }

    @Test
    public void rm() throws IOException {
        String hash = "QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH";
        Multihash filePointer = Multihash.fromBase58(hash);

        List<Multihash> rm = ipfs.pin.rm(filePointer);

        System.out.println(rm.get(0));  //返回结果文件内容的hash
    }

    @Test
    public void ls() throws IOException {
        String hash = "QmYHFcXuJzQxkmpZ7rQFQ1M6jsSBw3mCbshrqaHSe3LjrP";
        Multihash filePointer = Multihash.fromBase58(hash);

        Map ls = ipfs.file.ls(filePointer);
        // 打印值集合
        for (Object value : ls.values()) {
            System.out.println(value);
        }
    }

    @Test
    public void filesMkdir() throws IOException {
        NamedStreamable.FileWrapper file = new NamedStreamable.FileWrapper(new File("/home/leonard/code/metadata/test.txt"));
        MerkleNode addResult = ipfs.add(file).get(0);
        System.out.println(String.format("%s文件上传成功,Hash值为: %s", (Object) file.getName(), addResult.toString()));
    }


    /*
    http add
     */
    @Test
    public void httpAdd() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();
        String url = ipfHttpUrl + "add";
        String filePath = "/home/leonard/code/metadata/2.txt";
        String fileName = "1.txt";
        HttpPost post = new HttpPost(url);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);

        // 传文件
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addBinaryBody("file", new File(filePath), ContentType.DEFAULT_BINARY, fileName);
        builder.addTextBody("r", "true");
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /*
    http add  多个文件添加
     */
    @Test
    public void httpAddDir() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();
        String url = ipfHttpUrl + "add";
        String filePath = "/home/leonard/code/metadata/";
        String fileName1 = "2.json";
        String fileName2 = "2.png";
        HttpPost post = new HttpPost(url);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);

        // 传文件
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addBinaryBody("file1", new File(filePath+fileName1), ContentType.DEFAULT_BINARY, fileName1);
        builder.addBinaryBody("file2", new File(filePath+fileName2), ContentType.DEFAULT_BINARY, fileName2);
        builder.addTextBody("r", "true");


        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /*
    http files/mkdir
     */
    @Test
    public void httpMkdir() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/ipfsTest"));
        list.add(new BasicNameValuePair("parents", "true"));

        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mkdir params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "files/mkdir";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /*
    http files/ls
     */
    @Test
    public void httpLs() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/files"));
        list.add(new BasicNameValuePair("l", "true"));
//        list.add(new BasicNameValuePair("flush", "true"));
        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println(params);
        System.out.println("***************ls params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "files/ls";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /*
        http files/cp
    */
    @Test
    public void httpCp() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/ipfs/QmXxf6xTZc5u6RepqAcEpLVt9BSqw6zsjbUeG6djdJd3am"));
        list.add(new BasicNameValuePair("arg", "/ipfsTest/1.txt"));       //参数必须为arg
//        list.add(new BasicNameValuePair("flush", "true"));        //optional
        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************cp params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "files/cp";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /*
    http files/mv: unix的mv指令一样功能
     */
    @Test
    public void httpMvFile() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/ipfs/2.txt"));
        list.add(new BasicNameValuePair("arg", "/ipfsTest/1.txt"));
//        list.add(new BasicNameValuePair("flush", "true"));        //optional
        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "files/mv";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /*
    http files/mv: unix的mv指令一样功能
     */
    @Test
    public void httpMv() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/files"));
        list.add(new BasicNameValuePair("arg", "/filesMv"));
//        list.add(new BasicNameValuePair("flush", "true"));        //optional
        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "files/mv";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /*
    http name/publish: Publish an object to IPNS
     */
    @Test
    public void httpPublish() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(500000).setConnectTimeout(300000)
                .setConnectionRequestTimeout(300000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/ipfs/QmQJUTe9B3bQvoCU8nZbCgMjWqw7tbb48YNZ6gC81usVRU"));
        list.add(new BasicNameValuePair("key", "ipfsTest"));        //optional
//        list.add(new BasicNameValuePair("resolve", "true"));        //optional
//        list.add(new BasicNameValuePair("lifetime", "time"));        //optional
//        list.add(new BasicNameValuePair("ttl", "time"));        //optional

        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "name/publish";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /*
    http name/resolve
     */
    @Test
    public void httpResolve() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(50000).setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "/ipns/k51qzi5uqu5dh3669pqvx1zfgqajly99kszozdzyui8jkahl489zciwjhld4fn"));
//        list.add(new BasicNameValuePair("recursive", "true"));        //optional
//        list.add(new BasicNameValuePair("nocache", "true"));        //optional

        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "name/resolve";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /*
    http key/list
     */
    @Test
    public void httpKeyList() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(50000).setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
//        list.add(new BasicNameValuePair("recursive", "true"));        //optional
//        list.add(new BasicNameValuePair("nocache", "true"));        //optional

        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "key/list";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /*
    http key/gen
     */
    @Test
    public void httpKeyGen() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(50000).setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        //1、封装请求参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        list.add(new BasicNameValuePair("arg", "ipfsTest"));
//        list.add(new BasicNameValuePair("nocache", "true"));        //optional

        //2、转化参数
        String params = EntityUtils.toString(new UrlEncodedFormEntity(list, Consts.UTF_8));
        System.out.println("***************mv params: " + params);
        //3、创建HttpGet请求
        String url = ipfHttpUrl + "key/gen";
        HttpPost post = new HttpPost(url + "?" + params);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }
}


